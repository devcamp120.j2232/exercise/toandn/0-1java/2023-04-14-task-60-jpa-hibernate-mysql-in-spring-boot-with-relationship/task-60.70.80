package com.devcamp.orders.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orders.model.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Long> {
  Customer findByCustomerId(Long customerId);
}
