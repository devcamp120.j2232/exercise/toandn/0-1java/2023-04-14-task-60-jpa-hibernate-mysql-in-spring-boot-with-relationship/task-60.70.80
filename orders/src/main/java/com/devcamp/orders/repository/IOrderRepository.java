package com.devcamp.orders.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.orders.model.Order;

public interface IOrderRepository extends JpaRepository<Order, Long> {

}
