package com.devcamp.orders.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.orders.model.Customer;
import com.devcamp.orders.model.Order;
import com.devcamp.orders.repository.ICustomerRepository;

@Service
public class CustomerService {
  @Autowired
  ICustomerRepository pICustomerRepository;

  public ArrayList<Customer> getAllCustomers() {
    ArrayList<Customer> listCustomer = new ArrayList<>();
    pICustomerRepository.findAll().forEach(listCustomer::add);
    return listCustomer;
  }

  public Set<Order> getOrderByCustomerId(Long customerId) {
    Customer vCustomer = pICustomerRepository.findByCustomerId(customerId);
    if (vCustomer != null) {
      return vCustomer.getOrders();
    } else {
      return null;
    }
  }
}
