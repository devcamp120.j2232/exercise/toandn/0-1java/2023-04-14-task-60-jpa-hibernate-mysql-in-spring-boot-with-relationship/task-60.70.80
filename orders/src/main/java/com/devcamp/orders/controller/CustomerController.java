package com.devcamp.orders.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.orders.model.Customer;
import com.devcamp.orders.model.Order;
import com.devcamp.orders.service.CustomerService;
import com.devcamp.orders.service.OrderService;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CustomerController {
  @Autowired
  CustomerService customerService;

  @Autowired
  OrderService orderService;

  @GetMapping("/devcamp-customers")
  public ResponseEntity<List<Customer>> getAllCustomers() {
    try {

      return new ResponseEntity(customerService.getAllCustomers(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/devcamp-list-orders")
  public ResponseEntity<List<Order>> getAllOrders() {
    try {

      return new ResponseEntity(orderService.getAllOrders(), HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @GetMapping("/devcamp-orders")
  public ResponseEntity<Set<Order>> getOrderByCustomerId(@RequestParam(value = "customerId") Long customerId) {
    try {
      Set<Order> vOrder = customerService.getOrderByCustomerId(customerId);
      if (vOrder != null) {
        return new ResponseEntity<>(vOrder, HttpStatus.OK);
      } else {
        return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
